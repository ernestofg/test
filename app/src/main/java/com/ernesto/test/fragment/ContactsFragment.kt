package com.ernesto.test.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.Toast
import com.ernesto.test.LocalStorage
import com.ernesto.test.R
import com.ernesto.test.adapter.ContactAdapter
import com.ernesto.test.adapter.OnContactClickListener
import com.ernesto.test.pojo.Contact
import kotlinx.android.synthetic.main.fragment_contacts.*


class ContactsFragment : Fragment(), OnContactClickListener {

    private var adapter: ContactAdapter? = null
    private lateinit var searchView: SearchView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_contacts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerContacts.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
    }

    override fun onResume() {
        super.onResume()
        consultList()
    }

    private fun consultList() {
        adapter = ContactAdapter(LocalStorage.getContacts(), this)
        recyclerContacts.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        activity!!.menuInflater.inflate(R.menu.main, menu)
        val search = menu?.findItem(R.id.search)
        searchView = MenuItemCompat.getActionView(search) as SearchView
        search(searchView)
        consultList()
    }

    private fun search(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter?.filter?.filter(newText)
                return true
            }

        })

    }

    override fun onLongClick(contact: Contact) {
        AlertDialog.Builder(activity!!)
            .setTitle(getString(R.string.delete_contact_title))
            .setMessage(getString(R.string.delete_contact_message, contact.name))
            .setPositiveButton(getString(R.string.ok)) { dialog, which ->
                deleteContact(contact)
            }
            .setNegativeButton(getString(R.string.cancel))
            { dialog, which -> }
            .show()
    }

    private fun deleteContact(contact: Contact) {
        adapter?.items?.remove(contact)
        LocalStorage.setContacts(adapter!!.items)
        adapter?.notifyDataSetChanged()
        if(searchView.isIconfiedByDefault){
            adapter?.filter?.filter(searchView.query)
        }
        Toast.makeText(activity!!, getString(R.string.deleted), Toast.LENGTH_SHORT).show()
    }
}