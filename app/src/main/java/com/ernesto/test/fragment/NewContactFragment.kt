package com.ernesto.test.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ernesto.test.LocalStorage
import com.ernesto.test.R
import com.ernesto.test.pojo.Contact
import kotlinx.android.synthetic.main.fragment_new_contact.*

class NewContactFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_new_contact, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSave.setOnClickListener { saveContact() }
    }

    private fun saveContact() {
        if (edtUserName.text.toString().isEmpty() || edtLastName.text.toString().isEmpty()
            || edtAge.text.toString().isEmpty() || edtPhone.text.toString().isEmpty()
        ) {
            Toast.makeText(activity, getString(R.string.enter_data), Toast.LENGTH_SHORT).show()
            return
        }

        LocalStorage.addContact(
            Contact(
                edtUserName.text.toString(),
                edtLastName.text.toString(),
                edtAge.text.toString(),
                edtPhone.text.toString(),
                edtImageUrl.text.toString()
            )
        )

        Toast.makeText(activity, getString(R.string.saved), Toast.LENGTH_SHORT).show()

        edtUserName.text.clear()
        edtLastName.text.clear()
        edtAge.text.clear()
        edtPhone.text.clear()
        edtImageUrl.text.clear()

    }
}