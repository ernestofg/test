package com.ernesto.test.adapter

import com.ernesto.test.pojo.Contact

interface OnContactClickListener {
    fun onLongClick(contact: Contact)
}