package com.ernesto.test.adapter

import android.graphics.drawable.Drawable
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.ernesto.test.R
import com.ernesto.test.pojo.Contact
import kotlinx.android.synthetic.main.item_contact.view.*

class ContactAdapter(val items: ArrayList<Contact>, private val listener: OnContactClickListener) :
    RecyclerView.Adapter<ContactAdapter.ViewHolder>(), Filterable {

    private var itemsFiltered = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(itemsFiltered[position], listener, position)

    override fun getItemCount() = itemsFiltered.size

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    itemsFiltered = items
                } else {
                    val filteredList = ArrayList<Contact>()
                    for (item in items) {
                        if (item.name.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(item)
                        }
                    }
                    itemsFiltered = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = itemsFiltered
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                itemsFiltered = filterResults.values as ArrayList<Contact>
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Contact, listener: OnContactClickListener, position: Int) = with(itemView) {
            /*itemTitle.text = item.name
            itemImage.loadUrl(item.url)*/
            val txtName = "${item.name} ${item.lastName}"
            tvName.text = txtName
            tvAge.text = context.getString(R.string.age_field, item.age)
            tvPhone.text = context.getString(R.string.phone_field, item.phoneNumber)

            Glide.with(context)
                .load(item.url)
                .apply(RequestOptions.circleCropTransform())
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        Handler().post {
                            Glide.with(context)
                                .load(R.drawable.ic_profile)
                                .apply(RequestOptions.circleCropTransform())
                                .into(imgUser)
                        }
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                })
                .into(imgUser)

            setOnLongClickListener {
                listener.onLongClick(item)
                true
            }
        }

    }
}