package com.ernesto.test.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.ernesto.test.R
import com.ernesto.test.fragment.ContactsFragment
import com.ernesto.test.fragment.NewContactFragment


class CustomPagerAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ContactsFragment()
            else -> NewContactFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.title_list)
            1 -> context.getString(R.string.title_new_contact)
            else -> null
        }
    }
}