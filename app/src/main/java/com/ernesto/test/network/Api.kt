package com.ernesto.test.network

import com.ernesto.test.pojo.RequestLogin
import com.ernesto.test.pojo.ResponseLogin
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {
    @POST("test")
    fun login(@Body body: RequestLogin): Call<ResponseLogin>
}