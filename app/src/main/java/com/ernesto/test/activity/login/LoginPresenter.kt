package com.ernesto.test.activity.login

import com.ernesto.test.App
import com.ernesto.test.R

class LoginPresenter(private val view: LoginContract.View) : LoginContract.Presenter {

    private var model: LoginModel = LoginModel(this)

    override fun loginButtonClicked(user: String, pass: String) {
        if (user.isEmpty() || pass.isEmpty()) {
            view.showMessage(App.applicationContext().getString(R.string.enter_data))
            return
        }

        view.showProgress()
        model.requestLogin(user, pass)
    }

    override fun loginError(string: String) {
        view.dismissProgress()
        view.showMessage(string)
    }

    override fun loginSuccessful() {
        view.dismissProgress()
        view.loginSuccessful()
    }
}