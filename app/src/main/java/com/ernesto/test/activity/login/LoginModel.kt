package com.ernesto.test.activity.login

import com.ernesto.test.App
import com.ernesto.test.LocalStorage
import com.ernesto.test.R
import com.ernesto.test.network.BaseClient
import com.ernesto.test.pojo.RequestLogin
import com.ernesto.test.pojo.ResponseLogin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginModel(private val presenter: LoginContract.Presenter) : LoginContract.Model {

    override fun requestLogin(user: String, pass: String) {

        val call = BaseClient.provideApiService()
            .login(RequestLogin(user, pass))

        call.enqueue(object : Callback<ResponseLogin> {
            override fun onFailure(call: Call<ResponseLogin>, t: Throwable) {
                presenter.loginError(App.applicationContext().getString(R.string.error_login))
            }

            override fun onResponse(call: Call<ResponseLogin>, response: Response<ResponseLogin>) {
                if (response.body()?.statusCode == 200) {
                    val name = response.body()?.body?.auth?.user?.name
                    if (name != null) {
                        LocalStorage.setUserName(name)
                    }
                    presenter.loginSuccessful()
                } else
                    presenter.loginError(App.applicationContext().getString(R.string.error_login))
            }
        })
    }
}