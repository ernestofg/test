package com.ernesto.test.activity.login

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.ernesto.test.R
import com.ernesto.test.activity.MainActivity
import kotlinx.android.synthetic.main.activity_login_view.*


class LoginView : AppCompatActivity(), LoginContract.View {

    private lateinit var presenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_view)
        presenter = LoginPresenter(this)

        btnLogin.setOnClickListener {
            presenter.loginButtonClicked(edtUserName.text.toString(), edtPassword.text.toString())
        }
    }

    override fun showMessage(string: String) {
        Toast.makeText(this@LoginView, string, Toast.LENGTH_SHORT).show()
    }

    override fun loginSuccessful() {
        startActivity(MainActivity.newIntent(this))
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun dismissProgress() {
        progressBar.visibility = View.INVISIBLE
    }

}
