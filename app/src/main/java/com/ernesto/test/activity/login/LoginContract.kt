package com.ernesto.test.activity.login

interface LoginContract {
    interface View {
        fun showMessage(string: String)

        fun loginSuccessful()

        fun showProgress()

        fun dismissProgress()
    }

    interface Presenter {
        fun loginButtonClicked(user: String, pass: String)

        fun loginError(string: String)

        fun loginSuccessful()
    }

    interface Model {
        fun requestLogin(user: String, pass: String)
    }
}