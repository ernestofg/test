package com.ernesto.test

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.ernesto.test.pojo.Contact
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class LocalStorage {
    companion object {
        private val USER = "user"
        private val CONTACTS = "contacts"

        private fun sharedPreferences(): SharedPreferences {
            val context = App.applicationContext()
            return PreferenceManager.getDefaultSharedPreferences(context)
        }

        private fun saveString(key: String, value: String) {
            val editor = sharedPreferences().edit()
            editor.putString(key, value)
            editor.apply()
        }

        fun setUserName(string: String) {
            saveString(USER, string)
        }

        fun getUserName(): String {
            return sharedPreferences().getString(USER, "")
        }

        fun setContacts(body: List<Contact>) {
            val json: String
            if (body.isEmpty())
                json = ""
            else
                json = Gson().toJson(body)
            saveString(CONTACTS, json)
        }

        fun getContacts(): ArrayList<Contact> {
            val json = sharedPreferences().getString(CONTACTS, "")
            if (json!!.trim { it <= ' ' }.isEmpty()){
                val defaulList = obtainDefaultList()
                setContacts(defaulList)
                return defaulList
            }
            val type = object : TypeToken<List<Contact>>() {

            }.type
            return Gson().fromJson<ArrayList<Contact>>(json, type)
        }

        fun addContact(contact: Contact){
            val list = getContacts()
            list.add(contact)
            setContacts(list)
        }

        fun obtainDefaultList(): ArrayList<Contact> {
            val list = ArrayList<Contact>()
            list.add(Contact("Ernesto", "Flores", "18","333333333", "https://www.randomaddressgenerator.com/media/face/male56.jpg"))
            list.add(Contact("Juan", "Gaytan", "22", "4444444", "https://storage01.natives.zone/files/4/2663-5a76227604239.jpg"))
            list.add(Contact("Alejandra", "Lopez", "24", "55555555", "http://mavericklaw.ca/images/Steph_BW_SQ256.jpeg"))
            list.add(Contact("Yolanda", "Perez", "22", "777777777", "https://static1.squarespace.com/static/53d14d5ce4b0d5933c1dffa3/t/5b9a03ce0e2e72572882b1d3/1406225914691/jho-low-profile.png"))
            list.add(Contact("Ruben", "Flores", "22", "888888888", "https://media.licdn.com/dms/image/C5103AQF-tANxK-k6Fg/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=Kj_1EOSoEMSZQVN9pugadiV1NaoTnp8e9k3GLcuJw6w"))
            list.add(Contact("Ramona", "Rodriguez", "10", "999999999", "https://colorlib.com/polygon/gentelella/images/img.jpg"))
            list.add(Contact("Ana", "Gtz", "56", "100000000", "https://www.serviceprofessionalsnetwork.com/wp-content/uploads/avatars/251/5c02b96a39612-bpfull.jpg"))
            list.add(Contact("Cesar", "Smith", "33", "112312312312", "https://www.randomaddressgenerator.com/media/face/female94.jpg"))
            list.add(Contact("Uriel", "A", "33", "11111111111", "https://vets-now.s3.amazonaws.com/uploads/2017/02/IMG_6393-256x256.jpg"))
            list.add(Contact("Juliet", "Del Rio", "39","9090909090", "https://static1.squarespace.com/static/53d14d5ce4b0d5933c1dffa3/t/5b9a03ce0e2e72572882b1d3/1406225914691/jho-low-profile.png"))
            return list
        }
    }

}