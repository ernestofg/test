package com.ernesto.test.pojo

data class Contact(
    val name: String,
    val lastName: String,
    val age: String,
    val phoneNumber: String,
    val url: String = ""
)