package com.ernesto.test.pojo

import com.google.gson.annotations.SerializedName

class ResponseLogin {
    @SerializedName("statusCode")
    var statusCode: Int? = null
    @SerializedName("body")
    var body: Body? = null

    class Body {
        @SerializedName("status")
        var status: String? = null
        @SerializedName("auth")
        var auth: Auth? = null
    }

    class Auth {
        @SerializedName("user")
        var user: User? = null
        @SerializedName("access_token")
        var accessToken: String? = null
    }

    class User {
        @SerializedName("name")
        var name: String? = null
        @SerializedName("lastname")
        var lastName: String? = null
        @SerializedName("email")
        var email: String? = null
        @SerializedName("address")
        var address: String? = null
    }
}

